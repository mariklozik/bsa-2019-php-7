<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $fillable = ['name', 'surname', 'country', 'city', 'phone', 'address_line'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
