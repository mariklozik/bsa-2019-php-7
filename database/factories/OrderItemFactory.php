<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id'   => $faker->numberBetween(1, 10),
        'product_id' => $faker->numberBetween(1, 10),
        'quantity'   => $faker->numberBetween(1, 100),
        'price'      => $faker->numberBetween(100, 100000),
        'discount'   => $faker->numberBetween(0, 50),
        'sum'        => $faker->numberBetween(100, 100000)
    ];
});