<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['buyer_id'];

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }
}
